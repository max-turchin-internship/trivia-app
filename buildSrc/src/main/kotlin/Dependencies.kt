object BuildPlugins {

    object Versions {
        const val buildToolsVersion = "7.0.0-alpha03"
        const val kotlinVersion =  "1.4.20"
    }

    const val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.buildToolsVersion}"
    const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinVersion}"
    const val androidApplication = "com.android.application"
    const val kotlinAndroid = "kotlin-android"
}

object AndroidSdk {

    const val minimal = 27
    const val compile = 30
    const val target = compile
}

object Config {

    const val code = 1
    const val name = "1"

    const val testRunner = "androidx.test.runner.AndroidJUnitRunner"
}

object Compose {

    object Versions {
        const val composeVersion = "1.0.0-alpha07"
        const val kotlinCompiler = "1.4.10"
        const val navigation = "1.0.0-alpha02"
    }

    const val navigationCompose = "androidx.navigation:navigation-compose:${Versions.navigation}"
}
