package com.sannedak.triviaapp.data.repository

import com.sannedak.triviaapp.data.model.Question

class QuestionsRepository : QuestionListRepository {

    override fun getQuestions() = mutableListOf(
        Question(
            text = "What is Android Jetpack?",
            answers = listOf("all of these", "tools", "documentation", "libraries"),
            correctAnswer = "all of these"
        ),
        Question(
            text = "Base class for Layout?",
            answers = listOf("ViewGroup", "ViewSet", "ViewCollection", "ViewRoot"),
            correctAnswer = "ViewGroup"
        ),
        Question(
            text = "Layout for complex Screens?",
            answers = listOf("ConstraintLayout", "GridLayout", "LinearLayout", "FrameLayout"),
            correctAnswer = "ConstraintLayout"
        ),
        Question(
            text = "Pushing structured data into a Layout?",
            answers = listOf("Data Binding", "Data Pushing", "Set Text", "OnClick"),
            correctAnswer = "Data Binding"
        ),
        Question(
            text = "Inflate layout in fragments?",
            answers = listOf("onCreateView", "onActivityCreated", "onCreateLayout", "onInflateLayout"),
            correctAnswer = "onCreateView"
        ),
        Question(
            text = "Build system for Android?",
            answers = listOf("Gradle", "Graddle", "Grodle", "Groyle"),
            correctAnswer = "Gradle"
        ),
        Question(
            text = "Android vector format?",
            answers = listOf("VectorDrawable", "AndroidVectorDrawable", "DrawableVector", "AndroidVector"),
            correctAnswer = "VectorDrawable"
        ),
        Question(
            text = "Android Navigation Component?",
            answers = listOf("NavController", "NavCentral", "NavMaster", "NavSwitcher"),
            correctAnswer = "NavController"
        ),
        Question(
            text = "Registers app with launcher?",
            answers = listOf("intent-filter", "app-registry", "launcher-registry", "app-launcher"),
            correctAnswer = "intent-filter"
        ),
        Question(
            text = "Mark a layout for Data Binding?",
            answers = listOf("<layout>", "<binding>", "<data-binding>", "<dbinding>"),
            correctAnswer = "<layout>"
        )
    )
}
