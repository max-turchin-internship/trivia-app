package com.sannedak.triviaapp.data.repository

import com.sannedak.triviaapp.data.model.Question

interface QuestionListRepository {

    fun getQuestions(): List<Question>
}
