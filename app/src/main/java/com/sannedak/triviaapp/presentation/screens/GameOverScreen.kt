package com.sannedak.triviaapp.presentation.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.ConstraintLayout
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate
import androidx.navigation.compose.rememberNavController
import androidx.ui.tooling.preview.Preview
import com.sannedak.triviaapp.R
import com.sannedak.triviaapp.presentation.navigation.Screen
import com.sannedak.triviaapp.presentation.ui.*
import java.util.*

@Composable
fun GameOverScreen(navController: NavHostController) {
    Surface(color = gameOverBackground) {
        ConstraintLayout(modifier = Modifier.fillMaxSize()) {
            val (image, button) = createRefs()

            Image(
                asset = vectorResource(id = R.drawable.try_again),
                modifier = Modifier
                    .height(350.dp)
                    .constrainAs(image) {
                        start.linkTo(parent.start, margin = halfPadding)
                        end.linkTo(parent.end, margin = halfPadding)
                        top.linkTo(parent.top, margin = halfPadding)
                        bottom.linkTo(button.top, margin = padding)
                    }
            )
            Button(
                onClick = { navController.navigate(Screen.Title.route) },
                modifier = Modifier.constrainAs(button) {
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    top.linkTo(image.bottom)
                    bottom.linkTo(parent.bottom)
                }
            ) {
                Text(
                    text = stringResource(id = R.string.try_again).toUpperCase(Locale.getDefault()),
                    style = typography.button
                )
            }
        }
    }
}

@Preview
@Composable
private fun GameOverPreview() {
    val navController = rememberNavController()

    TriviaAppTheme {
        GameOverScreen(navController)
    }
}
