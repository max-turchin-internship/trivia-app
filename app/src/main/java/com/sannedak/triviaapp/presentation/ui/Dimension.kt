package com.sannedak.triviaapp.presentation.ui

import androidx.compose.ui.unit.dp

val bigPadding = 32.dp
val padding = 16.dp
val halfPadding = 8.dp
