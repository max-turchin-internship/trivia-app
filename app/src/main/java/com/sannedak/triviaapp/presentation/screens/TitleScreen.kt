package com.sannedak.triviaapp.presentation.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.ConstraintLayout
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate
import androidx.navigation.compose.rememberNavController
import androidx.ui.tooling.preview.Preview
import com.sannedak.triviaapp.R
import com.sannedak.triviaapp.presentation.navigation.Screen
import com.sannedak.triviaapp.presentation.ui.TriviaAppTheme
import com.sannedak.triviaapp.presentation.ui.halfPadding
import java.util.*

@Composable
fun TitleScreen(navController: NavHostController) {
    ConstraintLayout(modifier = Modifier.fillMaxSize()) {
        val (image, button) = createRefs()

        Image(
            asset = vectorResource(id = R.drawable.android_trivia),
            modifier = Modifier
                .height(192.dp)
                .constrainAs(image) {
                    start.linkTo(parent.start, margin = halfPadding)
                    end.linkTo(parent.end, margin = halfPadding)
                    top.linkTo(parent.top)
                    bottom.linkTo(button.top)
                }
        )
        Button(
            onClick = { navController.navigate(Screen.Game.route) },
            modifier = Modifier.constrainAs(button) {
                start.linkTo(parent.start, margin = halfPadding)
                end.linkTo(parent.end, margin = halfPadding)
                top.linkTo(image.bottom)
                bottom.linkTo(parent.bottom)
            }
        ) {
            Text(text = stringResource(id = R.string.play).toUpperCase(Locale.ROOT))
        }
    }
}

@Preview
@Composable
private fun TitlePreview() {
    val navController = rememberNavController()

    TriviaAppTheme {
        TitleScreen(navController)
    }
}
