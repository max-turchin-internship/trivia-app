package com.sannedak.triviaapp.presentation.screens.game

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.selection.selectable
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate
import androidx.navigation.compose.rememberNavController
import androidx.ui.tooling.preview.Preview
import com.sannedak.triviaapp.R
import com.sannedak.triviaapp.data.repository.QuestionsRepository
import com.sannedak.triviaapp.presentation.navigation.Screen
import com.sannedak.triviaapp.presentation.screens.game.model.UiQuestionsMapper
import com.sannedak.triviaapp.presentation.ui.*
import java.util.*

@Composable
fun Game(
    gameViewModel: GameViewModel,
    navController: NavHostController,
    appBarText: MutableState<String>
) {
    val questions = remember { mutableStateOf(gameViewModel.questionsList.value) }
    val questionIndex = remember { mutableStateOf(0) }

    onDispose { gameViewModel.addQuestions() }

    Column(modifier = Modifier.fillMaxSize()) {
        Image(
            asset = vectorResource(id = R.drawable.android_category_simple),
            modifier = Modifier
                .height(192.dp)
                .align(alignment = Alignment.CenterHorizontally)
                .padding(
                    start = halfPadding,
                    top = padding,
                    end = halfPadding,
                    bottom = padding
                )
        )
        Question(question = questions.value[questionIndex.value].text)
        Answers(
            answers = questions.value[questionIndex.value].answers,
            correctAnswer = questions.value[questionIndex.value].correctAnswer,
            questionIndex = questionIndex,
            navController = navController,
            appBarText = appBarText
        )
    }
}

@Composable
private fun Question(question: String) {
    Text(
        text = question,
        modifier = Modifier
            .padding(start = bigPadding, top = padding, end = bigPadding, bottom = padding),
        style = typography.h5.merge(TextStyle(fontWeight = FontWeight.Bold))
    )
}

@Composable
private fun Answers(
    answers: List<String>,
    correctAnswer: String,
    questionIndex: MutableState<Int>,
    navController: NavHostController,
    appBarText: MutableState<String>
) {
    val (selectedOption, onOptionSelected) = remember { mutableStateOf(answers[0]) }

    appBarText.value =
        stringResource(id = R.string.title_android_trivia_question) + " (${questionIndex.value + 1}/3)"

    Column(
        modifier = Modifier.padding(
            start = padding,
            end = padding,
            bottom = halfPadding
        )
    ) {
        answers.forEach { answer ->
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxWidth()
                    .selectable(
                        selected = (answer == selectedOption),
                        onClick = { onOptionSelected(answer) }
                    )
                    .padding(vertical = halfPadding)
            ) {
                RadioButton(
                    selected = (answer == selectedOption),
                    onClick = { onOptionSelected(answer) },
                    modifier = Modifier.padding(start = padding)
                )
                Text(
                    text = answer,
                    style = MaterialTheme.typography.body1,
                    modifier = Modifier
                        .padding(start = 16.dp)
                )
            }
        }
        Button(
            onClick = {
                if (selectedOption == correctAnswer) {
                    questionIndex.value++
                    if (questionIndex.value == 3) {
                        navController.navigate("Game Won/${questionIndex.value}")
                    }
                } else {
                    navController.navigate(Screen.GameOver.route)
                }
            },
            modifier = Modifier
                .padding(top = halfPadding)
                .align(Alignment.CenterHorizontally)
        ) {
            Text(text = stringResource(id = R.string.submit).toUpperCase(Locale.ROOT))
        }
    }
}

@Preview
@Composable
private fun GamePreview() {
    val repository = QuestionsRepository()
    val mapper = UiQuestionsMapper()
    val gameViewModel = GameViewModel(repository, mapper)
    val navController = rememberNavController()
    val appBarText = remember { mutableStateOf("Game") }

    TriviaAppTheme { Game(gameViewModel, navController, appBarText) }
}
