package com.sannedak.triviaapp.presentation

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.*
import androidx.compose.ui.platform.setContent
import com.sannedak.triviaapp.data.repository.QuestionsRepository
import com.sannedak.triviaapp.presentation.screens.TriviaApp
import com.sannedak.triviaapp.presentation.screens.game.GameViewModel
import com.sannedak.triviaapp.presentation.screens.game.model.UiQuestionsMapper
import com.sannedak.triviaapp.presentation.ui.TriviaAppTheme

class MainActivity : AppCompatActivity() {

    private val repository = QuestionsRepository()
    private val mapper = UiQuestionsMapper()
    private val gameViewModel: GameViewModel by viewModels {
        BaseViewModelFactory(this) { GameViewModel(repository, mapper) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TriviaAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    TriviaApp(gameViewModel)
                }
            }
        }
    }
}
