package com.sannedak.triviaapp.presentation.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.ConstraintLayout
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate
import androidx.navigation.compose.rememberNavController
import androidx.ui.tooling.preview.Preview
import com.sannedak.triviaapp.R
import com.sannedak.triviaapp.presentation.navigation.Screen
import com.sannedak.triviaapp.presentation.ui.*
import java.util.*

@Composable
fun GameWonScreen(navController: NavHostController, correctAnswers: Int?, shareState: MutableState<Int>) {
    shareState.value = correctAnswers ?: 3

    Surface(color = youWinBackground) {
        ConstraintLayout(modifier = Modifier.fillMaxSize()) {
            val (image, button) = createRefs()

            Image(
                asset = vectorResource(id = R.drawable.you_win),
                modifier = Modifier
                    .height(350.dp)
                    .constrainAs(image) {
                        start.linkTo(parent.start, margin = halfPadding)
                        end.linkTo(parent.end, margin = halfPadding)
                        top.linkTo(parent.top, margin = halfPadding)
                        bottom.linkTo(button.top, margin = padding)
                    }
            )
            Button(
                onClick = { navController.navigate(Screen.Title.route) },
                modifier = Modifier.constrainAs(button) {
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    top.linkTo(image.bottom)
                    bottom.linkTo(parent.bottom)
                }
            ) {
                Text(
                    text = stringResource(id = R.string.next_match).toUpperCase(Locale.getDefault()),
                    style = typography.button
                )
            }
        }
    }
}

@Preview
@Composable
private fun PreviewGameWon() {
    val navController = rememberNavController()
    val correctAnswers = 3
    val shareState = remember { mutableStateOf(3) }

    TriviaAppTheme {
        GameWonScreen(navController, correctAnswers, shareState)
    }
}
