package com.sannedak.triviaapp.presentation.navigation

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.navigation.NavBackStackEntry
import androidx.navigation.compose.KEY_ROUTE

fun NavBackStackEntry.getRoute() = arguments?.getString(KEY_ROUTE) ?: ""

sealed class Screen(val route: String) {
    object Title : Screen("Android Trivia")
    object Game : Screen("Game")
    object About : Screen("About")
    object Rules : Screen("Rules")
    object GameWon : Screen("Game Won/{numCorrect}")
    object GameOver : Screen("Game Over")

    fun saveState() = bundleOf(KEY_SCREEN to route)

    val id: Int
        get() {
            return route.hashCode() + 0x00010000
        }

    companion object {
        const val KEY_SCREEN = "route"

        fun restoreState(bundle: Bundle): Screen {
            return when (bundle.getString(KEY_SCREEN, Title.route)) {
                Title.route -> Title
                Game.route -> Game
                About.route -> About
                Rules.route -> Rules
                GameWon.route -> GameWon
                GameOver.route -> GameOver
                else -> Title
            }
        }
    }
}
