package com.sannedak.triviaapp.presentation.screens.game.model

import com.sannedak.triviaapp.data.model.Question

class UiQuestionsMapper {

    fun map(questions: List<Question>): List<UiQuestion> {
        return questions.map { UiQuestion(it.text, mapAnswers(it.answers), it.correctAnswer) }
    }

    private fun mapAnswers(answers: List<String>): List<String> {
        val shuffledAnswers = answers.toMutableList()
        shuffledAnswers.shuffle()
        return shuffledAnswers
    }
}
