package com.sannedak.triviaapp.presentation.screens.game

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.sannedak.triviaapp.data.repository.QuestionListRepository
import com.sannedak.triviaapp.presentation.screens.game.model.UiQuestion
import com.sannedak.triviaapp.presentation.screens.game.model.UiQuestionsMapper

class GameViewModel(
    private val repository: QuestionListRepository,
    private val mapper: UiQuestionsMapper
) : ViewModel() {

    var questionsList = mutableStateOf(listOf<UiQuestion>())
        private set

    init {
        addQuestions()
    }

    fun addQuestions() {
        val shadowQuestions = repository.getQuestions()
        val mappedQuestions = mapper.map(shadowQuestions).shuffled()
        questionsList.value = mappedQuestions
    }
}
