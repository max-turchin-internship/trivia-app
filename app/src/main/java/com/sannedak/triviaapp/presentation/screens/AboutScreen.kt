package com.sannedak.triviaapp.presentation.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import androidx.ui.tooling.preview.Preview
import com.sannedak.triviaapp.R
import com.sannedak.triviaapp.presentation.ui.TriviaAppTheme
import com.sannedak.triviaapp.presentation.ui.halfPadding
import com.sannedak.triviaapp.presentation.ui.padding
import com.sannedak.triviaapp.presentation.ui.typography

@Composable
fun AboutTriviaScreen() {
    Column(modifier = Modifier.fillMaxSize()) {
        Image(
            asset = vectorResource(id = R.drawable.about_android_trivia),
            modifier = Modifier
                .fillMaxWidth()
                .height(192.dp)
                .align(alignment = Alignment.CenterHorizontally)
                .padding(start = halfPadding, top = padding, end = halfPadding)
        )
        Text(
            text = stringResource(id = R.string.about_text),
            modifier = Modifier.padding(start = halfPadding, top = padding, end = halfPadding),
            style = typography.h5
        )
    }
}

@Preview
@Composable
private fun AboutPreview() {
    TriviaAppTheme {
        AboutTriviaScreen()
    }
}
