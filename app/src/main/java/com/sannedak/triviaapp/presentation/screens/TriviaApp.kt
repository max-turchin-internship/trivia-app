package com.sannedak.triviaapp.presentation.screens

import android.content.Intent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.VectorAsset
import androidx.compose.ui.platform.ContextAmbient
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.*
import com.sannedak.triviaapp.R
import com.sannedak.triviaapp.presentation.navigation.Screen
import com.sannedak.triviaapp.presentation.navigation.getRoute
import com.sannedak.triviaapp.presentation.screens.game.Game
import com.sannedak.triviaapp.presentation.screens.game.GameViewModel
import com.sannedak.triviaapp.presentation.ui.bigPadding
import com.sannedak.triviaapp.presentation.ui.halfPadding
import com.sannedak.triviaapp.presentation.ui.padding

@Composable
fun TriviaApp(gameViewModel: GameViewModel) {
    val navController = rememberNavController()

    val title = stringResource(id = R.string.android_trivia)
    val appBarText = remember { mutableStateOf(title) }
    val appBarState = remember { mutableStateOf(true) }
    val drawerVisibility = remember { mutableStateOf(true) }
    val shareState = remember { mutableStateOf(0) }

    val navHost = @Composable {
        NavHost(navController, startDestination = Screen.Title.route) {
            composable(Screen.Title.route) {
                TitleScreen(navController)
                appBarText.value = title
                appBarState.value = true
                drawerVisibility.value = true
            }
            composable(Screen.Game.route) {
                Game(gameViewModel, navController, appBarText)
                appBarState.value = false
                drawerVisibility.value = false
            }
            composable(Screen.About.route) {
                AboutTriviaScreen()
                appBarState.value = false
                drawerVisibility.value = false
            }
            composable(Screen.Rules.route) {
                RulesScreen()
                appBarState.value = false
                drawerVisibility.value = false
            }
            composable(
                Screen.GameWon.route,
                arguments = listOf(navArgument("numCorrect") { type = NavType.IntType })
            ) { backStackEntry ->
                GameWonScreen(navController, backStackEntry.arguments?.getInt("numCorrect"), shareState)
                appBarText.value = title
                appBarState.value = true
                drawerVisibility.value = false
            }
            composable(Screen.GameOver.route) {
                GameOverScreen(navController)
                appBarText.value = title
                drawerVisibility.value = false
            }
        }
    }

    AppScaffold(
        appBarState = appBarState,
        drawerVisibility = drawerVisibility,
        appBarText = appBarText,
        navController = navController,
        navHost = navHost,
        shareState = shareState
    )
}

@Composable
private fun AppScaffold(
    appBarState: MutableState<Boolean>,
    drawerVisibility: MutableState<Boolean>,
    appBarText: MutableState<String>,
    navController: NavHostController,
    navHost: @Composable () -> Unit,
    shareState: MutableState<Int>,
) {
    val drawerState = rememberDrawerState(DrawerValue.Closed)
    val currentScreen by navController.currentBackStackEntryAsState()
    val currentScreenName = currentScreen?.getRoute() ?: ""

    ModalDrawerLayout(
        drawerState = drawerState,
        gesturesEnabled = drawerVisibility.value,
        drawerContent = {
            TriviaAppDrawer(
                drawerState = drawerState,
                navController = navController
            )
        }
    ) {
        Column(modifier = Modifier.fillMaxSize()) {
            TopAppBar(
                title = { Text(text = appBarText.value) },
                navigationIcon = {
                    if (currentScreenName == "Android Trivia") {
                        TriviaAppBarIconButton(
                            icon = Icons.Outlined.Menu,
                            function = { drawerState.open() }
                        )
                    } else {
                        TriviaAppBarIconButton(
                            icon = Icons.Outlined.ArrowBack,
                            function = {
                                if (appBarState.value) {
                                    navController.popBackStack()
                                } else {
                                    navController.popBackStack(navController.graph.startDestination, false)
                                }
                            }
                        )
                    }
                },
                actions = { if (appBarState.value) ActionBarMenu(navController, shareState) }
            )
            navHost()
        }
    }
}

@Composable
private fun TriviaAppBarIconButton(icon: VectorAsset, function: () -> Unit) {
    IconButton(
        icon = { Icon(asset = icon) },
        onClick = function
    )
}

@Composable
private fun TriviaAppDrawer(drawerState: DrawerState, navController: NavHostController) {
    Column(modifier = Modifier.fillMaxSize()) {
        Surface(
            color = MaterialTheme.colors.primary,
            modifier = Modifier
                .fillMaxWidth()
                .height(192.dp)
        ) {
            Image(
                asset = vectorResource(id = R.drawable.nav_header),
                modifier = Modifier.padding(
                    start = halfPadding + padding,
                    top = halfPadding + padding,
                    end = halfPadding + padding,
                    bottom = bigPadding + padding
                )
            )
        }
        Column {
            DrawerListItem(
                icon = Icons.Outlined.Subject,
                text = stringResource(R.string.rules),
                drawerState = drawerState,
                navController = navController,
                route = Screen.Rules.route
            )
            DrawerListItem(
                icon = Icons.Outlined.Android,
                text = stringResource(R.string.about),
                drawerState = drawerState,
                navController = navController,
                route = Screen.About.route
            )
        }
    }
}

@Composable
private fun DrawerListItem(
    icon: VectorAsset,
    text: String,
    drawerState: DrawerState,
    navController: NavHostController,
    route: String
) {
    ListItem(
        icon = { Icon(asset = icon) },
        text = { Text(text = text) },
        modifier = Modifier.clickable(onClick = {
            navController.navigate(route)
            drawerState.close()
        })
    )
}

@Composable
private fun ActionBarMenu(navController: NavHostController, shareState: MutableState<Int>) {
    val showMenu = remember { mutableStateOf(false) }

    if (shareState.value != 3) {
        DropdownMenu(
            toggle = {
                IconButton(onClick = { showMenu.value = true }) {
                    Icon(Icons.Outlined.MoreVert)
                }
            },
            expanded = showMenu.value,
            onDismissRequest = { showMenu.value = false }
        ) {
            DropdownMenuItem(onClick = {
                navController.navigate(Screen.About.route)
                showMenu.value = false
            }) {
                Text(stringResource(id = R.string.about))
            }
        }
    } else {
        val context = ContextAmbient.current
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, stringResource(id = R.string.share_success_text))
            type = "text/plain"
        }

        val test = Intent.createChooser(sendIntent, null)
        IconButton(
            icon = { Icon(asset = Icons.Outlined.Share) },
            onClick = { startActivity(context, test, null) }
        )
    }
}
