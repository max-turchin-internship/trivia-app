package com.sannedak.triviaapp.presentation.screens.game.model

data class UiQuestion(
    val text: String,
    val answers: List<String>,
    val correctAnswer: String
)
