plugins {
    id(BuildPlugins.androidApplication)
    id(BuildPlugins.kotlinAndroid)
}

android {
    compileSdkVersion(AndroidSdk.compile)

    defaultConfig {
        applicationId = "com.sannedak.triviaapp"
        minSdkVersion(AndroidSdk.minimal)
        targetSdkVersion(AndroidSdk.target)
        versionCode = Config.code
        versionName = Config.name

        testInstrumentationRunner = Config.testRunner
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
        useIR = true
    }

    buildFeatures {
        compose = true
        viewBinding = true
    }

    composeOptions {
        kotlinCompilerExtensionVersion = Compose.Versions.composeVersion
        kotlinCompilerVersion = Compose.Versions.kotlinCompiler
    }
}

dependencies {

    // Kotlin
    implementation(Kotlin.stdlib)

    // AndroidX
    implementation(AndroidX.core)
    implementation(AndroidX.appCompat)
    implementation(AndroidX.lifecycle.runtimeKtx)

    // Jetpack Compose
    implementation(AndroidX.compose.ui)
    implementation(AndroidX.compose.material)
    implementation(AndroidX.ui.tooling)
    implementation(AndroidX.compose.runtime)
    implementation(Compose.navigationCompose) //refreshVersions doesn't know about this lib

    // Design
    implementation(Google.android.material)
    implementation(AndroidX.compose.material.icons.core)
    implementation(AndroidX.compose.material.icons.extended)

    // Tests
    testImplementation(Testing.junit)
    androidTestImplementation(AndroidX.test.ext.junitKtx)
    androidTestImplementation(AndroidX.test.espresso.core)
}